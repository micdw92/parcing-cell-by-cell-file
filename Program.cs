﻿using System;
using System.IO;
using System.Text;

namespace Parsing
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if ((args.Length == 0) || !File.Exists(args[0]))
                Console.WriteLine("Please provide a valid cell-by-cell file(.cbc) path");
            else
            {
                String modelName = Path.GetFileNameWithoutExtension(args[0]);

                int realBytes = 4;
                int ifDouble = 0;


                using (FileStream fs = new FileStream(args[0], FileMode.Open, FileAccess.Read))
                {
                    ReadingCBC(out ifDouble, fs, realBytes);
                }

                if (ifDouble == 1)
                {
                    realBytes = 8;

                    using (FileStream fs2 = new FileStream(args[0], FileMode.Open, FileAccess.Read))
                    {
                        ReadingCBC(out ifDouble, fs2, realBytes);
                    }
                }


            }
        }

        public static double ReadingReal(FileStream fs, int realBytes)
        {
            BinaryReader br = new BinaryReader(fs);

            byte[] data = br.ReadBytes(realBytes);
            double ddata = 0;

            if (realBytes == 4)
            {
                float fdata = BitConverter.ToSingle(data, 0);
                ddata = Convert.ToDouble(fdata);

            }
            else if (realBytes == 8)
                ddata = BitConverter.ToDouble(data, 0);
            else
                Console.WriteLine("[Warning] Errors in reading real values");

            return ddata;
        }

        public static void ReadingCBC(out int ifDouble, FileStream fs, int realBytes)
        {
            int NumCellsX;
            int NumCellsY;
            int NumCellsZ;
            int NumTimeSteps = 1;

            double[,,,] bufferMatrix = new double[1, 1, 1, 1];
            double[,,,] FlowFrontFace = new double[1, 1, 1, 1];
            double[,,,] FlowRightFace = new double[1, 1, 1, 1];
            double[,,,] FlowLowerFace = new double[1, 1, 1, 1];

            using (BinaryReader br = new BinaryReader(fs))
            {
                List<string> contents = new List<string>();
                List<string> doublecheck = new List<string>();
                doublecheck.Add("STORAGE");
                doublecheck.Add("CONSTANT_HEAD");
                doublecheck.Add("FLOW_RIGHT_FACE");
                doublecheck.Add("FLOW_FRONT_FACE");
                doublecheck.Add("FLOW_LOWER_FACE");

                int check = 1;
                ifDouble = 0;  //default: Real-value = Single
                int loopCount = 1;


                while (check != -1)
                {
                    int KSTP = br.ReadInt32();
                    int KPER = br.ReadInt32();

                    Console.WriteLine("Time step: {0}, Stress period: {1}", KSTP, KPER);
                    char[] DESC = br.ReadChars(16);
                    string tDESC = new string(DESC).Trim();
                    string sDESC = new string(tDESC).Replace(" ", "_");
                    if (!contents.Contains(sDESC))
                        contents.Add(sDESC);

                    int NCOL = br.ReadInt32();
                    NumCellsX = NCOL;
                    int NROW = br.ReadInt32();
                    NumCellsY = NROW;
                    int NLAY = br.ReadInt32();
                    NumCellsZ = Math.Abs(NLAY);

                    Console.WriteLine("# of Columns: {0}, # of Rows: {1}, # of Layers: {2}", NCOL, NROW, NLAY);

                    if (loopCount!=1 && contents[0] == sDESC)
                        NumTimeSteps++;

                    Console.WriteLine(sDESC);
                    Console.WriteLine(doublecheck.Contains(sDESC));

                    if (loopCount == 2 && !doublecheck.Contains(contents[1])) //check if the data type is double precision
                    {
                        ifDouble = 1;
                        Console.WriteLine("check value is {0}, and ifDouble value is {1}", check, ifDouble);
                        break;
                    }



                    if (NLAY > 0)
                    {
                        var Matrix = new double[NCOL, NROW, NLAY];

                        for (int i = 0; i < NLAY; i++)
                        {
                            for (int j = 0; j < NROW; j++)
                            {
                                for (int k = 0; k < NCOL; k++)
                                    Matrix[k, j, i] = ReadingReal(fs, realBytes);
                            }
                        }
                    }
                    else if (NLAY < 0)
                    {
                        int ITYPE = br.ReadInt32();
                        double DELT = ReadingReal(fs, realBytes);
                        double PERTIM = ReadingReal(fs, realBytes);
                        double TOTIM = ReadingReal(fs, realBytes);


                        Console.WriteLine("ITYPE: " + ITYPE + ", DELT: " + DELT + ", PERTIM: " + PERTIM + ", TOTIM: " + TOTIM);

                        int NVAL = 1; // the number of values associated with each cell (Assumption: Integer)

                        if (ITYPE == 5)
                        {
                            NVAL = br.ReadInt32();

                            if (NVAL > 1)
                            {
                                char[] CTMP = new char[16];

                                int i = 1;
                                do
                                {
                                    CTMP = br.ReadChars(16);
                                    i++;
                                    Console.WriteLine(CTMP);

                                } while (i <= NVAL - 1);
                            }
                            else
                                continue;
                        }
                        else
                            NVAL = 1;

                        Console.WriteLine("NVAL = {0}", NVAL);

                        //* Now read the data for each cells based on ITYPE *//

                        if (ITYPE == 0 || ITYPE == 1)
                        {
                            bufferMatrix = (double[,,,])ResizeArray(bufferMatrix, new int[] { NumCellsX, NumCellsY, NumCellsZ, 1 });

                            for (int i = 0; i < Math.Abs(NLAY); i++)
                            {

                                Console.WriteLine("{0,6} {1,4} {2,14} {3,14} {4,16} {5,5} {6,5} {7,5}", KSTP, KPER, PERTIM, TOTIM, sDESC, NCOL, NROW, (i + 1));

                                for (int j = 0; j < NROW; j++)
                                {
                                    Console.Write(" ");
                                    for (int k = 0; k < NCOL; k++)
                                    {
                                        bufferMatrix[k, j, i, 0] = ReadingReal(fs, realBytes);

                                        if (bufferMatrix[k, j, i, 0] >= 0)
                                            Console.Write(String.Format("{0:0.000000E+000} ", bufferMatrix[k, j, i, 0]));
                                        else
                                            Console.Write(String.Format("{0:0.00000E+000} ", bufferMatrix[k, j, i, 0]));
                                    }
                                    Console.Write("\n");
                                }
                            }
                        }
                        else if (ITYPE == 2 || ITYPE == 5)
                        {
                            int NLIST = br.ReadInt32();
                            bufferMatrix = (double[,,,])ResizeArray(bufferMatrix, new int[] { NumCellsX, NumCellsY, NumCellsZ, 1 });
                            Console.WriteLine("NLIST = {0}", NLIST);

                            if (NLIST > 0)
                            {
                                int NRC = NROW * NCOL;
                                int ICELL = 0;
                                double rNVAL = 0;

                                for (int i = 0; i < NLIST; i++)
                                {
                                    ICELL = br.ReadInt32();
                                    while (ICELL < 1)
                                        ICELL = br.ReadInt32();
                                    rNVAL = ReadingReal(fs, realBytes);

                                    int Layer = (int)MathF.Floor(((ICELL - 1) / NRC + 1));
                                    int Row = (int)MathF.Floor(((ICELL - (Layer - 1) * NRC) - 1) / NCOL + 1);
                                    int Column = (int)MathF.Floor(ICELL - (Layer - 1) * NRC - (Row - 1) * NCOL);

                                    Console.WriteLine("ICELL = {0}, Layer = {1}, Row = {2}, Column = {3}", ICELL, Layer, Row, Column);

                                    if (!Enumerable.Range(1, NumCellsZ).Contains(Layer) || !Enumerable.Range(1, NumCellsY).Contains(Row) || !Enumerable.Range(1, NumCellsX).Contains(Column))
                                    {
                                        goto Double;
                                    }

                                    if (bufferMatrix[(Column - 1), (Row - 1), (Layer - 1), 0] != 0)   // The case where more than 2 wells locate in one cell is considered.
                                        rNVAL = bufferMatrix[(Column - 1), (Row - 1), (Layer - 1), 0] + rNVAL;

                                    bufferMatrix[(Column - 1), (Row - 1), (Layer - 1), 0] = rNVAL;
                                }



                                for (int i = 0; i < Math.Abs(NLAY); i++)
                                {
                                    Console.WriteLine("{0,6} {1,4} {2,14} {3,14} {4,16} {5,5} {6,5} {7,5}", KSTP, KPER, PERTIM, TOTIM, sDESC, NCOL, NROW, (i + 1));

                                    for (int j = 0; j < NROW; j++)
                                    {
                                        Console.Write(" ");
                                        for (int k = 0; k < NCOL; k++)
                                        {
                                            if (bufferMatrix[k, j, i, 0] >= 0)
                                                Console.Write(String.Format("{0:0.000000E+000} ", bufferMatrix[k, j, i, 0]));
                                            else
                                                Console.Write(String.Format("{0:0.00000E+000} ", bufferMatrix[k, j, i, 0]));
                                        }
                                        Console.Write("\n");
                                    }
                                }

                            }
                            else if (NLIST == 0)
                            {
                                for (int i = 0; i < Math.Abs(NLAY); i++)
                                {
                                    Console.WriteLine("{0,6} {1,4} {2,14} {3,14} {4,16} {5,5} {6,5} {7,5}", KSTP, KPER, PERTIM, TOTIM, sDESC, NCOL, NROW, (i + 1));

                                    for (int j = 0; j < NROW; j++)
                                    {
                                        Console.Write(" ");
                                        for (int k = 0; k < NCOL; k++)
                                        {
                                            if (bufferMatrix[k, j, i, 0] >= 0)
                                                Console.Write(String.Format("{0:0.000000E+000} ", bufferMatrix[k, j, i, 0]));
                                            else
                                                Console.Write(String.Format("{0:0.00000E+000} ", bufferMatrix[k, j, i, 0]));
                                        }
                                        Console.Write("\n");
                                    }
                                }

                                Console.WriteLine("[Info] No Values");

                            }
                            else
                                Console.WriteLine("[Warning] NLIST < 0");

                        }
                        else if (ITYPE == 3) //Little confusing
                        {
                            if (Math.Abs(NLAY) == 1)
                            {
                                var values = new int[NROW, NCOL]; //layer indicator
                                for (int r = 0; r < NROW; r++)
                                {
                                    for (int c = 0; c < NCOL; c++)
                                        values[r, c] = br.ReadInt32();
                                }

                                bufferMatrix = (double[,,,])ResizeArray(bufferMatrix, new int[] { NumCellsX, NumCellsY, NumCellsZ, 1 });
                                Console.WriteLine("{0,6} {1,4} {2,14} {3,14} {4,16} {5,5} {6,5} {7,5}", KSTP, KPER, PERTIM, TOTIM, sDESC, NCOL, NROW, 1);

                                for (int l = 0; l < Math.Abs(NLAY); l++)
                                {
                                    for (int r = 0; r < NROW; r++)
                                    {
                                        Console.Write(" ");

                                        for (int c = 0; c < NCOL; c++)
                                        {
                                            bufferMatrix[c, r, l, 0] = ReadingReal(fs, realBytes);
                                            if (bufferMatrix[c, r, l, 0] >= 0)
                                                Console.Write(String.Format("{0:0.000000E+000} ", bufferMatrix[c, r, l, 0]));
                                            else
                                                Console.Write(String.Format("{0:0.00000E+000} ", bufferMatrix[c, r, l, 0]));
                                        }

                                        Console.Write("\n");

                                    }
                                }

                            }
                            else
                                Console.WriteLine("[Warning] ITYPE == 3 with more than 1 layer");

                        }
                        else if (ITYPE == 4)
                        {
                            bufferMatrix = (double[,,,])ResizeArray(bufferMatrix, new int[] { NumCellsX, NumCellsY, NumCellsZ, 1 });
                            int l = 0;

                            while (l < Math.Abs(NLAY))
                            {
                                Console.WriteLine("{0,6} {1,4} {2,14} {3,14} {4,16} {5,5} {6,5} {7,5}", KSTP, KPER, PERTIM, TOTIM, sDESC, NCOL, NROW, l + 1);

                                for (int r = 0; r < NROW; r++)
                                {
                                    Console.Write(" ");

                                    for (int c = 0; c < NCOL; c++)
                                    {
                                        if (l == 0)
                                            bufferMatrix[c, r, l, 0] = ReadingReal(fs, realBytes);

                                        if (bufferMatrix[c, r, l, 0] >= 0)
                                            Console.Write(String.Format("{0:0.000000E+000} ", bufferMatrix[c, r, l, 0]));
                                        else
                                            Console.Write(String.Format("{0:0.00000E+000} ", bufferMatrix[c, r, l, 0]));
                                    }
                                    Console.Write("\n");
                                }
                                l++;
                            }
                        }
                        else
                            Console.WriteLine("[WARNING} ITYPE value is not valid.");

                        FlowFrontFace = (double[,,,])ResizeArray(FlowFrontFace, new int[] { NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps });
                        FlowRightFace = (double[,,,])ResizeArray(FlowRightFace, new int[] { NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps });
                        FlowLowerFace = (double[,,,])ResizeArray(FlowLowerFace, new int[] { NumCellsX, NumCellsY, NumCellsZ, NumTimeSteps });

                        for (int i = 0; i < NumCellsX * NumCellsY * NumCellsZ; i++)
                        {
                            if (contents.Contains("FLOW_FRONT_FACE"))
                                FlowFrontFace[i % NumCellsX, (i % (NumCellsX * NumCellsY)) / NumCellsX, i / (NumCellsX * NumCellsY), NumTimeSteps - 1] = bufferMatrix[i % NumCellsX, (i % (NumCellsX * NumCellsY)) / NumCellsX, i / (NumCellsX * NumCellsY), 0];
                            if (contents.Contains("FLOW_RIGHT_FACE"))
                                FlowRightFace[i % NumCellsX, (i % (NumCellsX * NumCellsY)) / NumCellsX, i / (NumCellsX * NumCellsY), NumTimeSteps - 1] = bufferMatrix[i % NumCellsX, (i % (NumCellsX * NumCellsY)) / NumCellsX, i / (NumCellsX * NumCellsY), 0];
                            if (contents.Contains("FLOW_LOWER_FACE"))
                                FlowLowerFace[i % NumCellsX, (i % (NumCellsX * NumCellsY)) / NumCellsX, i / (NumCellsX * NumCellsY), NumTimeSteps - 1] = bufferMatrix[i % NumCellsX, (i % (NumCellsX * NumCellsY)) / NumCellsX, i / (NumCellsX * NumCellsY), 0];
                            bufferMatrix[i % NumCellsX, (i % (NumCellsX * NumCellsY)) / NumCellsX, i / (NumCellsX * NumCellsY), 0] = 0;
                        }
                    }

                    check = br.PeekChar();
                    while (check == 0)
                    {
                        int dump = br.ReadInt32();
                        check = br.PeekChar();
                    }
                    Console.WriteLine("termination value is {0}, and ifDouble value is {1}", check, ifDouble);

                    loopCount++;

                    Double:
                    {
                        if(realBytes != 8)
                        {
                            ifDouble = 1;
                            Console.WriteLine("The data is written in Double precision");
                            break;
                        }
                    }
                }
            }
        }

        private static double[,,,] ResizeArray(double[,,,] arr, int[] newSizes)
        {
            if (newSizes.Length != arr.Rank)
                throw new ArgumentException("arr must have the same number of dimensions " +
                                            "as there are elements in newSizes", "newSizes");

            var temp = new double[newSizes[0], newSizes[1], newSizes[2], newSizes[3]];
            for (int i = 0; i < arr.GetLength(3); i++)
                for(int j = 0; j < arr.GetLength(2); j++)
                    for(int k = 0; k < arr.GetLength(1); k++)
                        for(int l = 0; l < arr.GetLength(0); l++)
                            temp[l,k,j,i] = arr[l,k,j,i];

            return temp;
        }

    }
}
