# Simplest model for reference of building the binary-file-converting program
# Basic Package file created on 6/1/2022 by ModelMuse version 4.3.0.0.
# Number of active cells = 40.
FREE CHTOCH PRINTTIME  # OPTIONS
CONSTANT        1 # IBOUND Upper Aquifer
CONSTANT        1 # IBOUND Lower Aquifer
 -1.000000000000E+020  # HNOFLO
CONSTANT     2.000000000000E+001  # STRT Upper Aquifer
CONSTANT     2.000000000000E+001  # STRT Lower Aquifer
